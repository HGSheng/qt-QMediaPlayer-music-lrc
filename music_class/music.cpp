#include "music.h"
#include "ui_music.h"
#include <QDebug>

music::music(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::music)
{
    ui->setupUi(this);

    Init_all();

    QString path_mp3_1 = "C:/Users/Administrator/Desktop/test/蓝颜知己 - 丛浩楠.mp3";
    QString path_lrc_1 = "C:/Users/Administrator/Desktop/test/蓝颜知己 - 丛浩楠.lrc";
    QString path_mp3_2 = "/IOT/myqt/1.mp3";
    QString path_lrc_2 = "/IOT/myqt/1.lrc";
    qtaudioPlayer.setMedia(QUrl::fromLocalFile(path_mp3_1));
    if(!lyrics.readLyricsFile(path_lrc_1)){
        qDebug()<<("未检测到歌词文件");
        qDebug()<<("请检查歌词文件是否存在");
    }
    lyricsID = 0;

    connect(&qtaudioPlayer,SIGNAL(positionChanged(qint64)),this,SLOT(onPositionChanged(qint64)));
    connect(&qtaudioPlayer,SIGNAL(durationChanged(qint64)),this,SLOT(onDurationChanged(qint64)));
    qtaudioPlayer.play();
    run_statue = true;

}

music::~music()
{
    delete ui;
}

void music::Init_all()
{
    //页面大小
    this->setFixedSize(800, 480);
    this->move(0, 0);

    //水平靠右显示
    ui->music_time->setAlignment(Qt::AlignRight);

    //设置初始化音量
    set_volume(30);
    qtaudioPlayer.setVolume(get_volume());
    ui->Slider_volume->setMaximum(200);
    ui->Slider_volume->setMinimum(0);
    ui->Slider_volume->setValue(get_volume());
}
//设置音量大小
void music::set_volume(int v)
{
    this->current_volume = v;
}
int music::get_volume()
{
    return this->current_volume;
}

//----------------------槽函数----------------------------------------------
//设置进度条最大值 也就是歌曲时长 ms
void music::onDurationChanged(qint64 duration)
{
    ui->music_progress->setMaximum(duration);
    int secs = duration/1000; //全部秒数
    int mins = secs/60;//分
    secs = secs % 60;//秒
    durationTime = QString::asprintf("%d:%d",mins,secs);
    ui->music_time->setText(positionTime+"/"+durationTime);
}

//记录当前音乐进度
void music::onPositionChanged(qint64 position)
{
    if(ui->music_progress->isSliderDown()){
//        qDebug()<<"ui->music_progress->value() "<<ui->music_progress->value();
//        qDebug()<<"ui->music_progress->maximum() "<<ui->music_progress->maximum();
//        qtaudioPlayer.pause();
//        qtaudioPlayer.setPosition(ui->music_progress->value()*qtaudioPlayer.duration()/100);
//        position = ui->music_progress->value();
//        qtaudioPlayer.setPosition(position);
//        ui->music_progress->setSliderPosition(position);
//        qtaudioPlayer.play();
        return;
    }
        //如果手动调整进度条，则不处理
    ui->music_progress->setSliderPosition(position);
    current_value = position;
    int secs = position/1000;
    int mins = secs/60;
    secs = secs % 60;
    positionTime = QString::asprintf("%d:%d",mins,secs);
    ui->music_time->setText(positionTime+"/"+durationTime);

    if(!lyrics.getListLyricsTime().empty()&&qtaudioPlayer.position()>=lyrics.getListLyricsTime().at(lyricsID)&&lyricsID<lyrics.getListLyricsTime().size()-1){
        ui->textBrowser->append(lyrics.getListLyricsText().at(lyricsID));
        lyricsID++;
    }
//    ui->lblPlayTime->setText(this->settime(qtaudioPlayer.position()));
}


//播放进度

//静音或者开声音
void music::on_btn_music_volume_clicked()
{
    if(get_volume()){
        this->tmp_volume = get_volume();
        set_volume(0);
        qtaudioPlayer.setVolume(get_volume());
        ui->Slider_volume->setValue(get_volume());
    }
    else{
        set_volume(this->tmp_volume);
        qtaudioPlayer.setVolume(get_volume());
        ui->Slider_volume->setValue(get_volume());
    }
}
//音量进度条
void music::on_Slider_volume_valueChanged(int value)
{
//    qDebug()<<qtaudioPlayer.duration();
    set_volume(value);
    qtaudioPlayer.setVolume(get_volume());
}

//快退
void music::on_Fast_rewind_clicked()
{

    current_value = ui->music_progress->value();
    if(!(current_value > ui->music_progress->maximum()*0.05)){
        qtaudioPlayer.pause();
        current_value = 0;
        ui->music_progress->setSliderPosition(current_value);
        int secs = current_value/1000;
        int mins = secs/60;
        secs = secs % 60;
        positionTime = QString::asprintf("%d:%d",mins,secs);
        ui->music_time->setText(positionTime+"/"+durationTime);
        ui->textBrowser->clear();
        qtaudioPlayer.setPosition(current_value);
        qtaudioPlayer.play();
    }
    else
    {
        qtaudioPlayer.pause();
        current_value -= ui->music_progress->maximum()*0.05;
        ui->music_progress->setSliderPosition(current_value);
        int secs = current_value/1000;
        int mins = secs/60;
        secs = secs % 60;
        positionTime = QString::asprintf("%d:%d",mins,secs);
        ui->music_time->setText(positionTime+"/"+durationTime);
        ui->textBrowser->clear();
        qtaudioPlayer.setPosition(current_value);
        lyricsID = 0;
        int i = 0;
        while (i<current_value) {

            if(!lyrics.getListLyricsTime().empty()&&i>=lyrics.getListLyricsTime().at(lyricsID)&&lyricsID<lyrics.getListLyricsTime().size()-1){
                ui->textBrowser->append(lyrics.getListLyricsText().at(lyricsID));
                lyricsID++;
            }
            i++;
        }
        qtaudioPlayer.play();
    }
}
//上一曲
void music::on_last_music_clicked()
{

}
//暂停或者播放
void music::on_open_pause_clicked()
{
    if(run_statue){
        qtaudioPlayer.pause();
        current_value = ui->music_progress->value();
        run_statue = false;
    }
    else{
            qtaudioPlayer.setPosition(current_value);
            ui->music_progress->setSliderPosition(current_value);
            run_statue = true;
            qtaudioPlayer.play();
    }
}

//下一曲
void music::on_next_music_clicked()
{

}
//快进
void music::on_Fast_forward_clicked()
{
    current_value = ui->music_progress->value();
    if(((ui->music_progress->maximum()-current_value) < ui->music_progress->maximum()*0.05)){

             qtaudioPlayer.pause();
             current_value =ui->music_progress->maximum();
             ui->music_progress->setSliderPosition(current_value);
             int secs = current_value/1000;
             int mins = secs/60;
             secs = secs % 60;
             positionTime = QString::asprintf("%d:%d",mins,secs);
             ui->music_time->setText(positionTime+"/"+durationTime);
             qtaudioPlayer.setPosition(current_value);
             ui->textBrowser->clear();
             lyricsID = 0;
             int i = 0;
             while (i<current_value) {

                 if(!lyrics.getListLyricsTime().empty()&&i>=lyrics.getListLyricsTime().at(lyricsID)&&lyricsID<lyrics.getListLyricsTime().size()-1){
                     ui->textBrowser->append(lyrics.getListLyricsText().at(lyricsID));
                     lyricsID++;
                 }
                 i++;
             }
             qtaudioPlayer.play();
    }
    else
    {
        qtaudioPlayer.pause();
        current_value += ui->music_progress->maximum()*0.05;
        ui->music_progress->setSliderPosition(current_value);
        int secs = current_value/1000;
        int mins = secs/60;
        secs = secs % 60;
        positionTime = QString::asprintf("%d:%d",mins,secs);
        ui->music_time->setText(positionTime+"/"+durationTime);
        qtaudioPlayer.setPosition(current_value);
        ui->textBrowser->clear();
        lyricsID = 0;
        int i = 0;
        while (i<current_value) {

            if(!lyrics.getListLyricsTime().empty()&&i>=lyrics.getListLyricsTime().at(lyricsID)&&lyricsID<lyrics.getListLyricsTime().size()-1){
                ui->textBrowser->append(lyrics.getListLyricsText().at(lyricsID));
                lyricsID++;
            }
            i++;
        }
        qtaudioPlayer.play();
    }
}
//歌词
void music::on_music_lyric_clicked()
{

}
//选择音乐
void music::on_select_music_clicked()
{

}
