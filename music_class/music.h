#ifndef MUSIC_H
#define MUSIC_H

#include <QDialog>
#include <QMediaPlayer>
#include "lyrics.h"

namespace Ui {
class music;
}

class music : public QDialog
{
    Q_OBJECT

public:
    explicit music(QWidget *parent = nullptr);
    ~music();
    void Init_all();
    void set_volume(int v);
    int get_volume();
private slots:
    void on_btn_music_volume_clicked();
    void on_Slider_volume_valueChanged(int value);
    void on_Fast_rewind_clicked();
    void on_last_music_clicked();
    void on_open_pause_clicked();
    void on_next_music_clicked();
    void on_Fast_forward_clicked();
    void on_music_lyric_clicked();

private:
    Ui::music *ui;
    QMediaPlayer qtaudioPlayer;
    bool run_statue;    //运行状态，暂停还是播放
    bool lyrics_statue; //歌词状态
    qint64 max_value;
    qint64 current_value;
    int current_volume;     //当前音量
    int tmp_volume;         //临时备份音量

private:
        QString durationTime;
        QString positionTime;
        Lyrics lyrics;
        int lyricsID = 0;
        int tmp_lyricsID = 0;

private slots:
        void onDurationChanged(qint64 duration); //文件时长变化，更新当前播放文件名显示
        //播放文件数据总大小的信号， 它可以获得文件时间长度。
        void onPositionChanged(qint64 position); //当前文件播放位置变化，更新进度显示
        //播放到什么位置的信号， 参数是以毫秒来计算的。
        void on_select_music_clicked();
};

#endif // MUSIC_H
